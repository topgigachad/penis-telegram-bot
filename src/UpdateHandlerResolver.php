<?php

namespace PenisBot;

use PenisBot\Handlers\BotAddedToChat;
use PenisBot\Handlers\Error;
use PenisBot\Handlers\GigachadCommand;
use PenisBot\Handlers\HandlerInterface;
use PenisBot\Handlers\PenisCommand;
use PenisBot\Handlers\PidorCommand;
use PenisBot\Handlers\PrivateChat;
use PenisBot\Handlers\RegCommand;
use PenisBot\Handlers\TopGigachadCommand;
use PenisBot\Handlers\TopPenisCommand;
use PenisBot\Handlers\TopPidorCommand;
use Telegram\Bot\Objects\Update;

class UpdateHandlerResolver
{
    private Bot $bot;
    private PrivateChat $privateChat;
    private BotAddedToChat $botAddedToChat;
    private Error $error;
    private RegCommand $regCommand;
    private PenisCommand $penisCommand;
    private TopPenisCommand $topPenisCommand;
    private PidorCommand $pidorCommand;
    private TopPidorCommand $topPidorCommand;
    private GigachadCommand $gigachadCommand;
    private TopGigachadCommand $topGigachadCommand;

    public function __construct(
        Bot $bot,
        PrivateChat $privateChat,
        BotAddedToChat $botAddedToChat,
        Error $error,
        RegCommand $regCommand,
        PenisCommand $penisCommand,
        TopPenisCommand $topPenisCommand,
        PidorCommand $pidorCommand,
        TopPidorCommand $topPidorCommand,
        GigachadCommand $gigachadCommand,
        TopGigachadCommand $topGigachadCommand
    )
    {
        $this->bot = $bot;
        $this->privateChat = $privateChat;
        $this->botAddedToChat = $botAddedToChat;
        $this->error = $error;
        $this->regCommand = $regCommand;
        $this->penisCommand = $penisCommand;
        $this->topPenisCommand = $topPenisCommand;
        $this->pidorCommand = $pidorCommand;
        $this->topPidorCommand = $topPidorCommand;
        $this->gigachadCommand = $gigachadCommand;
        $this->topGigachadCommand = $topGigachadCommand;
    }

    public function resolve(Update $update): ?HandlerInterface
    {
        if ($this->isEmptyMessage($update)) {
            return null; // Пропускаем событие т.к. дальше обрабатываются только сообщения
        }

        if ($this->isPrivateChat($update)) {
            return $this->privateChat;
        }

        if ($this->isAddBotToChat($update)) {
            return $this->botAddedToChat;
        }

        if ($this->isRegCommand($update)) {
            if ($this->isEmptyUsername($update)) {
                return $this->error;
            }

            return $this->regCommand;
        }

        if ($this->isPenisCommand($update)) {
            return $this->penisCommand;
        }

        if ($this->isTopPenisCommand($update)) {
            return $this->topPenisCommand;
        }

        if ($this->isPidorCommand($update)) {
            return $this->pidorCommand;
        }

        if ($this->isTopPidorCommand($update)) {
            return $this->topPidorCommand;
        }

        if ($this->isGigachadCommand($update)) {
            return $this->gigachadCommand;
        }

        if ($this->isTopGigachadCommand($update)) {
            return $this->topGigachadCommand;
        }

        return null;
    }

    private function isEmptyMessage(Update $update): bool
    {
        return $update->getMessage() === null;
    }

    private function isPrivateChat(Update $update): bool
    {
        return $update->getMessage()->getChat()->getType() === 'private';
    }

    private function isAddBotToChat(Update $update): bool
    {
        return $update->getMessage()->getChat()->getType() === 'group' &&
            $update->getMessage()->getNewChatParticipant() !== null &&
            $update->getMessage()->getNewChatParticipant()->getUsername() === $this->bot->getName();
    }

    private function isEmptyUsername(Update $update): bool
    {
        return $update->getMessage()->getFrom() === null ||
            empty($update->getMessage()->getFrom()->getUsername());
    }

    private function isRegCommand(Update $update): bool
    {
        return $update->getMessage()->getChat()->getType() === 'group' &&
            str_starts_with($update->getMessage()->getText(), RegCommand::COMMAND_NAME);
    }

    private function isPenisCommand(Update $update): bool
    {
        return $update->getMessage()->getChat()->getType() === 'group' &&
            str_starts_with($update->getMessage()->getText(), PenisCommand::COMMAND_NAME);
    }

    private function isTopPenisCommand(Update $update): bool
    {
        return $update->getMessage()->getChat()->getType() === 'group' &&
            str_starts_with($update->getMessage()->getText(), TopPenisCommand::COMMAND_NAME);
    }

    private function isPidorCommand(Update $update): bool
    {
        return $update->getMessage()->getChat()->getType() === 'group' &&
            str_starts_with($update->getMessage()->getText(), PidorCommand::COMMAND_NAME);
    }

    private function isTopPidorCommand(Update $update): bool
    {
        return $update->getMessage()->getChat()->getType() === 'group' &&
            str_starts_with($update->getMessage()->getText(), TopPidorCommand::COMMAND_NAME);
    }

    private function isGigachadCommand(Update $update): bool
    {
        return $update->getMessage()->getChat()->getType() === 'group' &&
            str_starts_with($update->getMessage()->getText(), GigachadCommand::COMMAND_NAME);
    }

    private function isTopGigachadCommand(Update $update): bool
    {
        return $update->getMessage()->getChat()->getType() === 'group' &&
            str_starts_with($update->getMessage()->getText(), TopGigachadCommand::COMMAND_NAME);
    }
}
